var restify = require('restify');
var mongojs = require('mongojs');
 

// Server
var server = restify.createServer();
 

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.listen(3000, function () {
    console.log("Server started @ 3000");
});

//status
var db = mongojs('Twitterdb', ['status', 'comment','users','likes','follows']);

//post status
server.post('/status', function (req, res, next) {
    var product = req.params;
    db.status.save(product,
        function (err, data) {
            res.writeHead(200, {
                'Content-Type': 'application/json; charset=utf-8'
            });
            res.end(JSON.stringify(data));
        });
    return next();
});

//show all status
server.get('/status/all', function (req, res, next) {
    db.status.find(function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

//new feed
    server.get('/newfeed/:id', function (req, res, next) {
        
        db.follows.find({ followee_id : req.params.id},function (err, data) {console.log(data)
            if(err) {throw err;}
            else
            {
                if(!data)
                {res.send(403, {message : "you must follow some one"})}
                else {
                    followees = data.map(function(value) {
                return value.follower_id;
            });
                    console.log(followees, 'dm')
                    followees.push(req.params.id)

                    console.log(followees, 'a')
            db.status.find({ user_id: { $in : followees} },function (err,data2) {
                    res.end(JSON.stringify(data2)).sort({creatime : -1})
            
            
            ;})
        }
    }
        });
        return next();
    });


//del status
server.del('/status/:id', function (req, res, next) {
    db.status.remove({
        _id: req.params.id
    }, function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(true));
    });
    return next();
});

// show status
server.get('/status/:id', function (req, res, next) {
    db.status.findOne({
        _id: mongojs.ObjectId(req.params.id)
    }, function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

//edit status
server.put('/status/:id', function (req, res, next) {
    // get the existing product
    db.status.findOne({
        id: req.params.id
    }, function (err, data) {
        // merge req.params/product with the server/product
 
        var updProd = {}; // updated products 
        // logic similar to jQuery.extend(); to merge 2 objects.
        for (var n in data) {
            updProd[n] = data[n];
        }
        for (var n in req.params) {
            updProd[n] = req.params[n];
        }
        db.status.update({
            id: req.params.id
        }, updProd, {
            multi: false
        }, function (err, data) {
            res.writeHead(200, {
                'Content-Type': 'application/json; charset=utf-8'
            });
            res.end(JSON.stringify(data));
        });
    });
    return next();
});




//comment


// post comment.... body must have user_id comment and status_id
server.post('/comment', function (req, res, next) {
    var product = req.params;
    db.comment.save(product,
        function (err, data) {
            res.writeHead(200, {
                'Content-Type': 'application/json; charset=utf-8'
            });
            res.end(JSON.stringify(data));
        });
    return next();
});

//show comment
server.get('/comment/:id', function (req, res, next) {
    db.comment.findOne({
        _id: mongojs.ObjectId(req.params.id)
    }, function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

//show all comment of 1 status
server.get('/comment/all/:id', function (req, res, next) {
    
    db.comment.find({ status_id : req.params.id},function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

//del comment
server.del('/comment/:id', function (req, res, next) {
    db.comment.remove({
        _id: req.params.id
    }, function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(true));
    });
    return next();
});

//edit comment
server.put('/comment/:id', function (req, res, next) {
    // get the existing product
    db.comment.findOne({
        id: req.params.id
    }, function (err, data) {
        // merge req.params/product with the server/product
 
        var updProd = {}; // updated products 
        // logic similar to jQuery.extend(); to merge 2 objects.
        for (var n in data) {
            updProd[n] = data[n];
        }
        for (var n in req.params) {
            updProd[n] = req.params[n];
        }
        db.comment.update({
            id: req.params.id
        }, updProd, {
            multi: false
        }, function (err, data) {
            res.writeHead(200, {
                'Content-Type': 'application/json; charset=utf-8'
            });
            res.end(JSON.stringify(data));
        });
    });
    return next();
});




//user
//sign up


// server.post('/user', function (req, res, next) {
//     var product = req.params;

//     db.user.save(product,
//         function (err, data) {
//             res.writeHead(200, {
//                 'Content-Type': 'application/json; charset=utf-8'
//             });
//             res.end(JSON.stringify(data));
//         });
//     return next();
// });
// server.post('/user', function(req, res, next) {
//         var username = req.body.username,
//             password = req.body.password,
//             newUser;

//         if (username.length === 0 || password.length === 0) {
//             res.send({success: false, message: 'Username or password hasn\'t been input.'});
//             return next();
//         }

//        db.user.findOne({username: username}, function(err, user) {
//             if (err) throw err;

//             if (!!user) {
//                 res.send({success: false, message: 'Input username has been used.'});
//                 return next();
//             }

//             newUser = new User({
//                 username: username,
//                 password: password,
//                 avatar: ""
//             });

//             // newUser.save(function(err) {
//             //     if (err) throw err;

//             //     res.send({success: true, message: 'Registered successfully!', username: username});
//             // });

 // db.user.save(User,
 //        function (err, data) {
 //             res.writeHead(200, {
 //                'Content-Type': 'application/json; charset=utf-8'
 //            });
 //            res.end(JSON.stringify(data));
 //        });
 //    return next();
 // });
 //            next();
 //        });
 //    });

server.post('/user', function (req, res, next) {
    var user = req.params;

    if (user.username.length === 0 || user.password.length === 0) {
        res.send(403, { message: 'Username or password hasn\'t been input.' });
        return next();
    }

    db.users.findOne({username: user.username}, function (err, data) {
        if (err) throw err;

        if (!!data) {
            res.send(400, { message: 'A user with this username already exists' });
            return next();
        }

     

        db.users.save(user, function (err, data2) {
            if (err) { // duplicate key error
                throw err;
            } else {
                res.send(200, { message: 'Registered successfully!' });
            }
        });
    });        
    return next();
});

//show all user
server.get('/user/all', function (req, res, next) {
    db.users.find(function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

//del user
server.del('/user/:id', function (req, res, next) {
    db.users.remove({
        _id: mongojs.ObjectId(req.params.id)
    }, function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(true));
    });
    return next();
});

//show user
server.get('/user/:id', function (req, res, next) {
    db.users.findOne({
        _id: mongojs.ObjectId(req.params.id)
    }, function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

// edit user
server.put('/user/:id', function (req, res, next) {
    // get the existing product
    db.users.findOne({
        id: req.params.id
    }, function (err, data) {
        // merge req.params/product with the server/product
 
        var updProd = {}; // updated products 
        // logic similar to jQuery.extend(); to merge 2 objects.
        for (var n in data) {
            updProd[n] = data[n];
        }
        for (var n in req.params) {
            updProd[n] = req.params[n];
        }
        db.users.update({
            id: req.params.id
        }, updProd, {
            multi: false
        }, function (err, data) {
            res.writeHead(200, {
                'Content-Type': 'application/json; charset=utf-8'
            });
            res.end(JSON.stringify(data));
        });
    });
    return next();
});



//sign in
        server.post('/user/signin', function (req, res, next) 
        {
            if (req.body.username.trim().length == 0 || req.body.password.trim().length == 0) 
            {
                   res.writeHead(403, 
                   {
                       'Content-Type': 'application/json; charset=utf-8'
                  });          res.end(JSON.stringify
                  ({
                       error: "You must fill in username and password part"
                   }));      
                     }
            db.users.findOne({
                username: req.body.username}, 
                function (err, data) { if (err) { // duplicate key error
                        throw err;
                    } else {
                        if(req.body.password.toString().localeCompare(data.password.toString())==0)
                        {res.send(200,{message: 'login successfully'});} 
                }})
            //             if(req.body.password == users.password){res.send(200,{message: 'login successfully'})


    });






//like and unlike
//like 
//like and unlike post
    server.post('/status/like/:status_id', function (req, res, next) {
        var savelike = req.params; // client need an user_id to put in server
        db.status.findOne({_id : mongojs.ObjectId(req.params.status_id) }, function (err, data) {
            if (err) throw err;

            if (!data) {
                res.send(404, { message: "Status not found!" });
                return next();
            }

            
 db.likes.findOne({
        status_id: req.params.status_id,
        user_id: req.body.user_id
    }, function (err, data) {
        console.log(data)
        if(err) {throw err;} else{
        if(!data){db.likes.save(savelike, function (err, data){
                if (err) {throw err;}
                else { res.send(200, {message:'liked', data: data})}
            });}
            else{console.log(data)
        db.likes.remove(data)
        res.send(200, {message : "Unliked"})};}
        // console.log(data)
        // db.likes.remove(data)
        // res.send(200, {message : "Unliked"})};
    });            
            // db.likes.save(savelike, function (err, data){
            //     if (err) {throw err;}
            //     else { res.send(200, {message:'liked', data: data})}
            // });

            // db.status.update({ _id: mongojs.ObjectId(req.params.id) }, data, function (err) {
            //     if (err) throw err;
            //     res.send(200, { success: true, message: 'Like post successfully!' });
            // });
        });

        return next();
    });

//show all like 
server.get('/status/like/all/:status_id', function (req, res, next) {
    db.likes.find({
        status_id: req.params.status_id
    }, function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

    // Unlike
//     server.del('status/like/:id', function (req, res, next) {
//     db.likes.findOne({
//         status_id: req.params.id,
//         user_id: req.body.user_id
//     }, function (err, data) {if(err) {throw err;} else{
//         console.log(data)
//         db.likes.remove(data)
//         res.send(200, {message : "Unliked"})};
//     });
//     return next();
// });
//     // server.put('/like/:id', function (req, res, next) {
//     // // get the existing product
//     // db.likes.findOne({
//     //     id: req.params.id,
//     //     user_id: req.body.user_id
//     // }, function (err, data) {
//     //     if (!data)
//     //         {throw err}
//     //     else(db.likes.remove(data))
//         // merge req.params/product with the server/product
 
//         // var updProd = {}; // updated products 
//         // // logic similar to jQuery.extend(); to merge 2 objects.
//         // for (var n in data) {
//         //     updProd[n] = data[n];
//         // }
//         // for (var n in req.params) {
//         //     updProd[n] = req.params[n];
//         // }
//         // db.comment.update({
//         //     id: req.params.id
//         // }, updProd, {
//         //     multi: false
//         // }, function (err, data) {
//         //     res.writeHead(200, {
//         //         'Content-Type': 'application/json; charset=utf-8'
//         //     });
//         //     res.end(JSON.stringify(data));
//         // });
//     });
//     return next();
// });
 

 //follow
//following and unfollow
server.post('/follow/:follower_id', function (req, res, next) {
    
    db.users.findOne({ _id: mongojs.ObjectId(req.params.follower_id)
    }, function (err, data) {
        if(err) {throw err;}
        else
        {
            if(data)
            db.follows.findOne({
                follower_id: req.params.follower_id,
                followee_id: req.body.followee_id
            }, function (err,data2){
                if(err){throw err;}
                else
                {
                    if(!data2)
                    {
                        console.log('save');
                        db.follows.save({
                follower_id: req.params.follower_id,
                followee_id: req.body.followee_id
            });
                        res.send(200,{message: "Following"})
                    }
                    else
                    {
                        db.follows.remove(data2)
                        res.send(200, {message : "Unfollowed"})
                    }

                }
            })
        }
    });
    return next();
});


//show followee
server.get('/follow/followee/all/:follower_id', function (req, res, next) {
    db.follows.find({
        follower_id: req.params.follower_id
    }, function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

//show follower
server.get('/follow/follower/all/:followee_id', function (req, res, next) {
    db.follows.find({
        followee_id: req.params.followee_id
    }, function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

//show follows
server.get('/follow/all', function (req, res, next) {
    db.follows.find(function (err, data) {
        res.writeHead(200, {
            'Content-Type': 'application/json; charset=utf-8'
        });
        res.end(JSON.stringify(data));
    });
    return next();
});

module.exports = server;